# Purpose #
This process demonstrates how to install a docker image based on centos6.  If successful the image will have downloaded, via svn, qpid-cpp, built, run unit-tests, and installed the Qpid CPP broker.
# Process #
1. Clone the dockerfiles repository.
> $ sudo git clone https://bitbucket.org/elsammons/dockerfiles.git
1. Build the docker image
> $ cd dockerfiles/centos6qpidserver/
>
> $ sudo docker build .
1. With the image built you can now proceed to run the image, but first be sure you have enabled ip_forward for ease of use.  **Note** the Successfully built line, the image number that follows can be used later with the docker run command.  Do not worry if you loose track of this number, we can get it back...
>  Removing intermediate container eda2e44d99e7
>
>  Step 20 : EXPOSE 5672
>
>  ---> Running in 9607d432670b
>
>  ---> d9cbe0345e54
>
>  Removing intermediate container 9607d432670b
>
>  Successfully built **d9cbe0345e54**
1. Verify ip_forward is enabled and | or enable it.
>  $ sysctl net.ipv4.ip_forward
>
>  net.ipv4.ip_forward = 0
>
>  $ sudo sysctl net.ipv4.ip_forward=1
>
>  net.ipv4.ip_forward = 1
1. Get the image information and start qpidd, the broker.  We will start the broker with the **-t** option to attach a tty and qpidd will use the **-t** flag so that we can see trace information in real time.  If you are comfortable doing so you may drop these two flags and the image will run in attached mode.  See step 5.1 for an example of detach mode.
> $ sudo docker images
>
> REPOSITORY                                                  TAG                IMAGE ID            CREATED            VIRTUAL SIZE
>
> <none>                                                      <none>              d9cbe0345e54        17 minutes ago      2.34 GB
>
> <none>                                                      <none>              590e391076be        53 minutes ago      2.187 GB
>
> <none>                                                      <none>              41106cfa987a        About an hour ago  1.295 GB 
>
>$ sudo docker run -p 5672:5672 -t d9cbe0345e54 /usr/local/sbin/qpidd -t
    1. To run your image as detached simply use the **-d** option.
> $ sudo docker run -p 5672:5672 -d  d9cbe0345e54 /usr/local/sbin/qpidd
>
> a04e7ca51c93efd37f251cb4bc1040993cc96eb084a9fe5c2764d6d3f19d248c
1. To stop verify your image is running and to stop it simply use the ps and stop commands for docker.
> $ sudo docker ps
>
> CONTAINER ID        IMAGE                                                              COMMAND                CREATED             STATUS        PORTS                    NAMES
> 
> a04e7ca51c93        d9cbe0345e54                                                       "/usr/local/sbin/qpi   4 minutes ago       Up 4 minutes        0.0.0.0:5672->5672/tcp   silly_hawking
>
> $ sudo docker stop a04e
> 
> a04e
> 
> $ sudo docker ps
>
> CONTAINER ID        IMAGE                                                              COMMAND                CREATED             STATUS              PORTS               NAMES
> 